﻿using System;

namespace PR1
{
    public static class RandomGenerator
    {
        private static Random _rnd = new Random();
        public static int[] GenerateInt(int count, int min, int max)
        {
            int[] arr = new int[count];

            for(int i = 0; i < count; i++)
            {
                arr[i] = _rnd.Next(min, max);
            }

            return arr;
        }
        public static char[] GenerateChar(int count, int min, int max)
        {
            char[] arr = new char[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = Convert.ToChar(_rnd.Next(char.MinValue, max));
            }

            return arr;
        }

        public static float[] GenerateFloat(int count, double min, double max)
        {
            float[] arr = new float[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = (float)(_rnd.NextDouble() * (max - min) + min);
            }

            return arr;
        }
    }
}
