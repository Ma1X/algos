﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PR1
{
    public static class Sorter
    {
        public static int[] ArrayHeapSort(int[] arr)
        {
            Console.Write($"Heapsort\n");
            int n = arr.Length;

            for (int i = n / 2 - 1; i >= 0; i--)
                heapify(arr, n, i);

            for (int i = n - 1; i >= 0; i--)
            {
                int temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;

                heapify(arr, i, 0);
            }

            return arr;
        }

        public static float[] ShellSort(float[] a)
        {
            Console.Write($"Heapsort {a.Length}\n");
            float temp;
            int h = 0;;
            while (h <= a.Length / 3)
                h = 2 * h + 1;

            for (int k = h; k > 0; k = (k - 1) / 3)
            {
                for (int i = k; i < a.Length; i++)
                {
                    temp = a[i];
                    int j;
                    for (j = i; j >= k; j -= k)
                    {
                        if (temp < a[j - k])
                            a[j] = a[j - k];
                        else
                            break;
                    }
                    a[j] = temp;
                }
            }
            return a;
        }

        static void heapify(int[] arr, int n, int i)
        {
            int largest = i;
            int l = 2 * i + 1;
            int r = 2 * i + 2;

            if (l < n && arr[l] > arr[largest])
                largest = l;

            if (r < n && arr[r] > arr[largest])
                largest = r;

            if (largest != i)
            {
                int swap = arr[i];
                arr[i] = arr[largest];
                arr[largest] = swap;

                heapify(arr, n, largest);
            }
        }

        public static char[] ArrayCountingSort(char[] arr)
        {
            int min = -200, max = 10;

            Dictionary<int, char> counts = new Dictionary<int, char>();
            Console.Write($"Counting sort {arr.Length}\n");

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }

                if (arr[i] > max)
                {
                    max = arr[i];
                }

                char count;

                // If the key is not present, count will get the default value for int, i.e. 0
                counts.TryGetValue(arr[i], out count);
                counts[Convert.ToInt32(arr[i])] = Convert.ToChar(count + 1);
            }

            int k = 0;

            for (int j = min; j <= max; j++)
            {
                char count;

                if (counts.TryGetValue(j, out count))
                {
                    for (int i = 0; i < count; i++)
                    {
                        arr[k++] = Convert.ToChar(j);
                    }
                }
            }
            return arr;
        }
    }
}
