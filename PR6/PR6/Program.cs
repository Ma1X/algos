﻿using System;

namespace PR1
{
    class Program
    {
        static void Main()
        {
            ExperimentEnv.ArrayHeap(Sorter.ArrayHeapSort);
            ExperimentEnv.ArrayShell(Sorter.ShellSort);
            ExperimentEnv.ArrayCounting(Sorter.ArrayCountingSort);

            Console.ReadLine();
        }
    }
}
