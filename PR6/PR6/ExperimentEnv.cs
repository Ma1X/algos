﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PR1
{
    public static class ExperimentEnv
    {
        static Stopwatch _sw = new Stopwatch();
        static int[] _arraySizes = new int[]
        {
           10, 100, 500, 1000, 2000, 5000, 10000
        };
        public static event Action<int, long, string> ArraysSorted;

        static ExperimentEnv()
        {
            ArraysSorted += Notification;
        }

        public static void ArrayHeap(Func<int[], int[]> sortFunction)
        {
            int[] arr;

            foreach (var count in _arraySizes)
            {
                Console.Write($"Experiment with array ({count} items). Sort method - ");
                arr = RandomGenerator.GenerateInt(count, 0, 100);

                _sw.Start();
                var sortedArray = sortFunction(arr);
                _sw.Stop();

                ArraysSorted(count, _sw.ElapsedMilliseconds, "Array");
                _sw.Reset();
            }

            Console.WriteLine("\n");
        }

        public static void ArrayShell(Func<float[], float[]> sortFunction)
        {
            float[] arr;

            foreach (var count in _arraySizes)
            {
                Console.Write($"Experiment with array ({count} items). Sort method - ");
                arr = RandomGenerator.GenerateFloat(count, 0, 300);

                _sw.Start();
                var sortedArray = sortFunction(arr);
                _sw.Stop();

                ArraysSorted(count, _sw.ElapsedMilliseconds, "Array");
                _sw.Reset();
            }

            Console.WriteLine("\n");
        }

        public static void ArrayCounting(Func<char[], char[]> sortFunction)
        {
            char[] arr;

            foreach (var count in _arraySizes)
            {
                Console.Write($"Experiment with array ({count} items). Sort method - ");
                arr = RandomGenerator.GenerateChar(count, -200, 10);

                _sw.Start();
                var sortedArray = sortFunction(arr);
                _sw.Stop();

                ArraysSorted(count, _sw.ElapsedMilliseconds, "Array");
                _sw.Reset();
            }

            Console.WriteLine("\n");
        }

        private static void Notification(int count, long time, string item) 
            => Console.WriteLine($"{item} with {count} items was sorted for {time} milliseconds\n");
    }
}
