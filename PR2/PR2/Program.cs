﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PR2
{
    class Program
    {
        public static double m = Math.Pow(2, 31) - 1;
        public static double a = 16807, c = 0, x = 1;
        public static int steps = 30000, max = 200;
        static void Main(string[] args)
        {
            double[] generatedNumbers = new double[steps];
            Generate(ref generatedNumbers);
            PrintArray(generatedNumbers);
            EveryNumber(generatedNumbers);
            Console.WriteLine($"Шанс появи випадкового числа = {Convert.ToDouble(max) / Convert.ToDouble(steps) * 100}%");
            Console.WriteLine($"Середнє число = {Avg(generatedNumbers)}");
            Console.WriteLine($"Дисперсiя = {Dispersion(generatedNumbers)}");
            Console.WriteLine($"Вiдхилення = {Math.Sqrt(Dispersion(generatedNumbers))}");

            Console.ReadLine();
        }
        static void Generate(ref double[] arr)
        {

            for (int i = 0; i < steps; i++)
            {
                x = (a * x + c) % m;
                arr[i] = x % (max);
            }
        }
        static double Avg(double[] arr)
        {
            double sum = 0;
            foreach(var item in arr)
            {
                sum += item;
            }

            return sum / arr.Length;
        }
        static double Dispersion(double[] arr)
        {
            double sumD = 0, avg = Avg(arr);
            foreach(var item in arr)
            {
                sumD += (item - avg) * (item - avg);
            }
            return sumD / arr.Length;
        }

        static void PrintArray(double[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if(i % 14 == 0)
                    Console.Write($"{arr[i]}\n");
                else
                    Console.Write($"{arr[i]}\t");

            }  
        }

        static Dictionary<double, int> EveryNumber(double[] arr)
        {
            Dictionary<double, int> dict = new Dictionary<double, int>();

            for (int i = 0; i < 200; i++)
                dict.Add(i, 
                    arr.Where(x => Convert.ToInt32(x) == i).Count());
            for (int i = 0; i < dict.Count; i++)
            {
                Console.WriteLine($"Value {i} -> {(double)dict[i]}");
            }
                
            return dict;
        }
    }
}
