﻿using System;
using System.Diagnostics;

namespace PR3_2_R
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Fibonacci(90);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            
            Console.ReadLine();
            sw.Restart();
            Bubble(Rnd(1000));
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.ReadLine();
        }

        private static void Fibonacci(int n)
        {
            ulong current = 0, prev = 0, beforePrev = 0;

            for(int i = 0; i < n; i++)
            {
                if(i == 0)
                {
                    current = 0;
                }
                else if(i == 1)
                {
                    current = 1;
                    beforePrev = prev;
                    prev = current;
                }
                else
                {
                    current = beforePrev + prev;
                    beforePrev = prev;
                    prev = current;
                }
                //Console.WriteLine(current);
            }
        }
        private static void Bubble (float[] arr)
        {
            float c;
            for (int b = 0; b < arr.Length; b++)
            {
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    if (arr[i] < arr[i + 1])
                    {
                        c = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = c;
                    }
                }
            }
            foreach(var item in arr)
            {
                //Console.WriteLine(item);
            }
        }
        private static float[] Rnd(int n)
        {
            float[] arr = new float[n];
            Random rnd = new Random();
            for(int i = 0; i < n; i++)
            {
                arr[i] = (float)rnd.NextDouble();
            }
            return arr;
        }
    }
}
