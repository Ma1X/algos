﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR3_1_R
{
    public static class Environment
    {
        private static int _step = 1;
        private static int _maxValue = 50;

        public static void Tabulate(Func<int, double> func)
        {
            for(int n = 0; n <= _maxValue; n += _step)
            {
                Console.WriteLine($"{n} {func(n)}");
            }
        }
    }
}
