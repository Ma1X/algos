﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR3_1_R
{
    public static class Tabulator
    {
        public static double n(int n) => n;
        public static double log(int n) => Math.Log(n);
        public static double nlog(int n) => n * Math.Log(n);
        public static double n2(int n) => n * n;
        public static double _2n(int n) => Math.Pow(2, n);
        public static double fact(int n) => Factorial(n);


        private static int Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }
    } 
}
