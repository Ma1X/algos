﻿using System;

namespace PR3_1_R
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, double>[] tabFuncs =
            {
                Tabulator.n,
                Tabulator.n2,
                Tabulator.log,
                Tabulator.nlog,
                Tabulator._2n,
                Tabulator.fact
            };

            foreach(var func in tabFuncs)
            {
                Environment.Tabulate(func);
            }
        }
    }
}
