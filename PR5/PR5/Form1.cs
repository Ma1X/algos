﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;
using System.IO;


namespace algo5_6
{

    public partial class Form1 : Form
    {


        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeConsole();

        string str;

        static void Fill(ref int[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(0, 300);
        }
        static void FillF(ref float[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(-100, 10);
        }



        public class Node
        {
            public int data;
            public Node next, prev;
        }

        static Node GetNode(int data)
        {
            Node newNode = new Node();

            newNode.data = data;
            newNode.prev = newNode.next = null;
            return newNode;

        }

        static Node SortInsert(Node list, Node newNode)
        {
            Node current;

            if (list == null)
                list = newNode;

            else if (list.data >= newNode.data)
            {
                newNode.next = list;
                newNode.next.prev = newNode;
                list = newNode;
            }

            else
            {
                current = list;

                while (current.next != null && current.next.data < newNode.data)
                    current = current.next;

                newNode.next = current.next;

                if (current.next != null)
                    newNode.next.prev = newNode;

                current.next = newNode;
                newNode.prev = current;

            }
            return list;
        }

        static void Insertion(ref int[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                int j;
                int t = arr[i];

                for (j = i - 1; j >= 0; j--)
                {
                    if (arr[j] < t)
                        break;
                    arr[j + 1] = arr[j];
                }

                arr[j + 1] = t;
            }
        }

        static void Selection(ref int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int min = i;

                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[j] < arr[min])
                        min = j;
                }
                int t = arr[i];
                arr[i] = arr[min];
                arr[min] = t;
            }
        }



        double[] timesviborarr = new double[6];
        double[] timesvstavkaarr = new double[6];
        double[] timesvstavkalist = new double[6];

        public Form1()
        {


            InitializeComponent();
            while (true)
            {

                AllocConsole();
                int[] arr10 = new int[10];
                int[] arr100 = new int[100];
                int[] arr500 = new int[500];
                int[] arr1000 = new int[1000];
                int[] arr2000 = new int[2000];
                int[] arr5000 = new int[5000];
                float[] frr10 = new float[10];
                float[] frr100 = new float[100];
                float[] frr500 = new float[500];
                float[] frr1000 = new float[1000];
                float[] frr2000 = new float[2000];
                float[] frr5000 = new float[5000];


                Stopwatch time;

                Console.WriteLine("Сортування вибором (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Selection(ref arr10);
                }
                time.Stop();
                timesviborarr[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 10 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Selection(ref arr100);
                }
                time.Stop();

                timesviborarr[1] = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Час для масиву з 100 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Selection(ref arr500);
                }
                time.Stop();
                timesviborarr[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 500 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Selection(ref arr1000);
                }
                time.Stop();
                timesviborarr[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 1000 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Selection(ref arr2000);
                }
                time.Stop();
                timesviborarr[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 2000 елеметами = {time.Elapsed.TotalSeconds} s");



                Console.WriteLine();
                Console.WriteLine("Сортування вставками (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Insertion(ref arr10);
                }
                time.Stop();

                timesvstavkaarr[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Insertion(ref arr100);
                }
                time.Stop();
                timesvstavkaarr[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Insertion(ref arr500);
                }
                time.Stop();
                timesvstavkaarr[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Insertion(ref arr1000);
                }
                time.Stop();
                timesvstavkaarr[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Insertion(ref arr2000);
                }
                time.Stop();
                timesvstavkaarr[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");




                Console.WriteLine();

                Console.WriteLine("Сортування вставками (структура даних - список):");

                Random rnd = new Random();
                Node list = null;

                time = Stopwatch.StartNew();
                for (int i = 0; i < 10; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-10, 37)));
                }
                time.Stop();
                timesvstavkalist[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 100; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-10, 37)));
                }
                time.Stop();
                timesvstavkalist[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 500; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-10, 37)));
                }
                time.Stop();
                timesvstavkalist[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 1000; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-10, 37)));
                }
                time.Stop();
                timesvstavkalist[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 2000; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-10, 37)));
                }
                time.Stop();
                timesvstavkalist[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");

            }
        }

            public void button1_Click(object sender, EventArgs e)
            {

            }

        }

}




